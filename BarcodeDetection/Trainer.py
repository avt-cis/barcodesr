'''
@Description: The main routine for object detection.
@Developed by: Alex Choi
@Date: Aug. 03, 2021
@Contact: alex.choi@cognex.com
'''

#%% Import Libraries
import os
from tqdm import tqdm
import torch.utils.data

from module.engine import train_one_epoch, evaluate
import module.utils as utils
import module.transforms as T
from options import model_options
from data import CustomDataset, get_data_loader
from models import get_model
from utils import (
    get_current_datetime,
    get_device,
    get_configurations,
    get_output_file_paths,
    draw_bounding_boxes
)


#%% Configurations
YAML_CONFIG_PATH = "config/configs.yml"


#%% Training and evaluation functions
def get_transform(train):
    transforms = []
    transforms.append(T.ToTensor())

    if train:
        transforms.append(T.RandomHorizontalFlip(0.5))

    return T.Compose(transforms)


#%% function: main
def main():
    datetime_now = get_current_datetime()

    ## Getting parameters
    configs = get_configurations(YAML_CONFIG_PATH)

    ## Makikng output directories
    model_root_path, \
    model_save_path, \
    test_result_image_save_path, \
    bbox_cropped_image_dir = get_output_file_paths(
        configs=configs,
        datetime_now=datetime_now
    )

    ## Setting the compute device
    device = get_device(compute_device=configs["device"])

    #####################################################################################
    # TRAINING
    #####################################################################################

    # Defining dataset
    dataset_train = CustomDataset(
        annot_path=configs['data']['annot_json_path'],
        mode='train',
        transforms=get_transform(train=True)
    )

    dataset_valid = CustomDataset(
        annot_path=configs['data']['annot_json_path'],
        mode='valid',
        transforms=get_transform(train=False)
    )

    ## Defining training and validation data loaders
    dl_train = get_data_loader(
        dataset=dataset_train,
        batch_size=configs['train']['batch_size'],
        shuffle=configs['data']['shuffle']['train'],
        num_workers=configs['data_pipeline']['num_workers'],
        pin_memory=configs['data_pipeline']['pin_memory'],
        collate_fn=utils.collate_fn
    )

    dl_valid = get_data_loader(
        dataset=dataset_valid,
        batch_size=configs['valid']['batch_size'],
        shuffle=configs['data']['shuffle']['valid'],
        num_workers=configs['data_pipeline']['num_workers'],
        pin_memory=configs['data_pipeline']['pin_memory'],
        collate_fn=utils.collate_fn
    )

    ## Model
    num_classes = dataset_train.num_classes
    lr_scheduler_options = configs['train']['lr_scheduler']

    # Getting the model
    model, optimizer, learning_rate_scheduler = get_model(
        model_option=model_options[configs['train']['pretrained_model']],
        num_classes=num_classes,
        device=device,
        optimizer=configs['train']['optimizer'],
        lr_scheduler_options=lr_scheduler_options,
        pretrained=True
    )

    ## Train
    for epoch in range(configs['train']['epochs']):
        # train for one epoch, printing every 10 iterations
        train_one_epoch(
            model,
            optimizer,
            dl_train,
            device,
            epoch,
            print_freq=10
        )
        # update the learning rate
        learning_rate_scheduler.step()
        # evaluate on the test dataset
        evaluate(model, dl_valid, device=device)

    ## Save the trained model
    model_file_path = os.path.join(model_save_path, configs['outputs']['model_name'])
    torch.save(model, model_file_path)

    #####################################################################################
    # TESTING
    #####################################################################################

    ## Loading the model
    model = torch.load(model_file_path)

    dataset_test = CustomDataset(
        annot_path=configs['data']['annot_json_path'],
        mode='test',
        transforms=get_transform(train=False)
    )

    ## Drawing the bounding boxes for prediction
    ## Drawing the bounding boxes for prediction
    for idx in tqdm(range(len(dataset_test)), position=0, leave=True):
        draw_bounding_boxes(
            model=model,
            dataset=dataset_test,
            test_result_image_save_path=test_result_image_save_path,
            bbox_cropped_image_dir=bbox_cropped_image_dir,
            device=device,
            idx=idx,
            margin=configs['test']['bbox_pixel_margin'],
            threshold=configs['test']['threshold']
        )


#%% Main
if __name__ == "__main__":
    main()