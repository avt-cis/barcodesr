'''
@Description: The module for the object detection DNN models.
@Developed by: Alex Choi
@Date: Aug. 03, 2021
@Contact: cinema4dr12@gamil.com
'''

#%% Importing libraries
import torch
import torchvision
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torch.optim import lr_scheduler
from options import (
    model_options,
    optimizer_options
)


#%% Getting the model
def get_model(
    model_option: str,
    num_classes: int,
    device: str,
    optimizer: str,
    lr_scheduler_options: str,
    pretrained: bool=True,
) -> (torchvision.models.detection.faster_rcnn, torch.optim, torch.optim.lr_scheduler):

    """ Gets the model

        Params
        --------
            model_option (str): Selected model architecture
            num_classes (int): Number of classes
            device (str): Computing device (cpu / gpu)
            optimizer (str): Optimizer selected
            lr_scheduler_options (str):
            pretrained (bool): Option to use pretrained model

        Returns
        --------
            model (torchvision.models.detection.faster_rcnn): Faster R-CNN model
            optimizer (torch.optim): Optimizer object
            learning_rate_scheduler (torch.optim.lr_scheduler): Learning rate scheduler object

    """

    # load an instance segmentation model pre-trained on COCO
    model = model_option(pretrained=pretrained)

    # get the number of input features for the classifier
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    # replace the pre-trained head with a new one
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)

    model = model.to(device)

    # construct an optimizer
    params = [p for p in model.parameters() if p.requires_grad]

    optimizer = optimizer_options[optimizer](
        params,
        lr=0.005,
        momentum=0.9,
        weight_decay=0.0005
    )

    if lr_scheduler_options['algorithm'] == 'StepLR':
        learning_rate_scheduler = lr_scheduler.StepLR(
            optimizer,
            step_size=lr_scheduler_options['StepLR']['step_size'],
            gamma=lr_scheduler_options['StepLR']['decay_rate']
        )
    elif lr_scheduler_options['algorithm'] == 'ReduceLROnPlateau':
        learning_rate_scheduler = lr_scheduler.ReduceLROnPlateau(
            optimizer,
            factor=lr_scheduler_options['ReduceLROnPlateau']['factor'],
            patience=lr_scheduler_options['ReduceLROnPlateau']['patience']
        )
    else:
        pass

    return model, optimizer, learning_rate_scheduler