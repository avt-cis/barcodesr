'''
@Description: Module for utility functions.
@Developed by: Alex Choi
@Date: Aug. 04, 2021
@Contact: cinema4dr12@gmail.com
'''


#%% Import libraries
import os
import yaml
import time
from datetime import datetime
from logger import logging

from PIL import Image
from PIL import ImageDraw

import torch

import matplotlib.pyplot as plt
plt.interactive(False)


# %% Gets the current datetime
def get_current_datetime() -> str:

    """ Gets the current datetime

        Params
        --------
            None

        Returns
        --------
            datetime_now (str): Current datetime in string format

    """

    datetime_now = str(datetime.now())
    datetime_now = datetime.strptime(datetime_now, '%Y-%m-%d %H:%M:%S.%f')
    datetime_now = '{:04d}{:02d}{:02d}-{:02d}{:02d}{:02d}'.format(
        datetime_now.year,
        datetime_now.month,
        datetime_now.day,
        datetime_now.hour,
        datetime_now.minute,
        datetime_now.second
    )

    return datetime_now


# %% device settings
def get_device(compute_device: str) -> str:

    """ Gets compute device

    Params
    --------
        compute_device (str): Device name

    Returns
    --------
        device (str): Compute device (cpu or cuda)

    """

    # Available device
    device_available = 'cuda' if torch.cuda.is_available() else 'cpu'

    if compute_device == 'cpu':
        device = 'cpu'
    elif compute_device == 'gpu':
        if device_available == 'cuda': device = 'cuda'
    else:  # default option
        device = device_available

    logging.info(f'[Compute Device] Device: {device}')

    return device


# %% function to get the configurations from YAML
def get_configurations(config_yaml_path: str) -> dict:

    """ Gets compute device

    Params
    --------
        config_yaml_path (str): YAML configuration path

    Returns
    --------
        configs (dict): Configurations read from YAML

    """

    with open(config_yaml_path) as f:
        configs = yaml.load(f, Loader=yaml.FullLoader)

    if os.name == 'nt':
        configs['data_pipeline']['num_workers'] = 0

    return configs


# %% function: get_output_file_paths
def get_output_file_paths(
        configs: dict,
        datetime_now: str
) -> (str, str, str, str, str):

    """ Gets the paths of the output files

    Params
    --------
        config (dict): Configurations
        datetime_now (str): Current datetime

    Returns
    --------
        model_save_path (str): Root path to model file to save
        datetime_now_root_path (str): Root path to the output of the current model training
        test_result_image_save_path (str): Root path to test result images
        bbox_cropped_image_dir (str): Root path to bounding box cropped images

    """

    output_base_path = configs['outputs']['root_output_path']
    if not os.path.exists(output_base_path):
        os.mkdir(output_base_path)

    model_root_path = os.path.join(output_base_path, configs["model_name"])
    if not os.path.exists(model_root_path):
        os.mkdir(model_root_path)

    datetime_now_root_path = os.path.join(model_root_path, datetime_now)
    if not os.path.exists(datetime_now_root_path):
        os.mkdir(datetime_now_root_path)

    model_save_path = os.path.join(datetime_now_root_path, configs["outputs"]["model_save_dir"])
    test_result_image_save_path = os.path.join(datetime_now_root_path, configs["outputs"]["test_result_image_dir"])
    bbox_cropped_image_dir = os.path.join(datetime_now_root_path, configs["outputs"]["bbox_cropped_image_dir"])

    # if paths do not exist create them
    if not os.path.exists(model_save_path):
        os.makedirs(model_save_path)

    if not os.path.exists(test_result_image_save_path):
        os.makedirs(test_result_image_save_path)

    if not os.path.exists(bbox_cropped_image_dir):
        os.makedirs(bbox_cropped_image_dir)

    return \
        datetime_now_root_path, \
        model_save_path, \
        test_result_image_save_path, \
        bbox_cropped_image_dir


#%% function to draw a rectangle on the image
def pil_draw_rect(
    image: Image,
    x1: int, y1: int,
    x2: int, y2: int
) -> Image:

    """ Draws a rectangle on the image

        Params
        --------
            image (PIL.Image): Pillow image on which a rectangle is drawn
            x1 (int): x coordinate of the top-left point
            y1 (int): y coordinate of the top-left point
            x2 (int): x coordinate of the bottom-right point
            y2 (int): y coordinate of the bottom-right point

        Returns
        --------
            image (PIL.image): Image with rectangle drawn

    """

    draw = ImageDraw.Draw(image)
    draw.rectangle(
        ((x1, y1), (x2, y2)),
        outline=(255, 255, 0),
        width=2
    )

    return image


#%% function to draw the bounding boxes and crop with them
def draw_bounding_boxes(
    model,
    dataset,
    test_result_image_save_path,
    bbox_cropped_image_dir,
    device,
    idx,
    margin=0,
    test_only=False,
    threshold=0.5
):

    """ Gets data loader from the dataset

        Params
        --------
            model (): Trained model
            dataset (torch.utils.data.dataset): dataset
            test_result_image_save_path (str): Path to save the test result images
            bbox_cropped_image_dir (str): Path to save the cropped images from bounding boxes
            device (str): Computing device (cpu / gpu)
            idx (int): Index of the image in the dataset
            margin (int): Pixel margin for image cropping with the bounding boxes
            test_only (bool): Test only mode if True
            threshold (float): Score threshold for prediction

        Returns
        --------
            None

    """

    if test_only:
        img = dataset[idx]
        file_path = os.path.join(test_result_image_save_path, dataset.imgs[idx].split('/')[-1])
    else:
        img, _ = dataset[idx]
        file_path = os.path.join(
            test_result_image_save_path,
            dataset.imgs[idx]['file_path'].split('/')[-1]
        )

    # Putting the model in evaluation mode
    model.eval()

    start = time.time()
    with torch.no_grad():
        prediction = model([img.to(device)])
    end = time.time()
    print("\nElapsed: {:5.3f}(ms)".format(1000 * (end - start)))

    image_bb = Image.open(dataset.imgs[idx]).convert("RGB")
    image = Image.open(dataset.imgs[idx]).convert("RGB")

    for pred in prediction:
        for pred_idx, score in enumerate(pred['scores']):
            if score.detach().cpu().numpy() < threshold:
                continue

            box_array = pred['boxes'][pred_idx].detach().cpu().numpy()
            pt1 = (int(box_array[0]), int(box_array[1]))
            pt2 = (int(box_array[2]), int(box_array[3]))

            # crop the bounding boxes
            cropped = image.crop((
                pt1[0] - margin,
                pt1[1] - margin,
                pt2[0] + margin,
                pt2[1] + margin
            ))

            splitted = os.path.splitext(dataset.imgs[idx].split('/')[-1])
            cropped_file_path = os.path.join(
                bbox_cropped_image_dir,
                f"{splitted[0]}_cropped-{pred_idx}{splitted[1]}"
            )
            cropped.save(cropped_file_path)

            # draw the bounding boxes on the image
            image_bb = pil_draw_rect(image_bb, pt1[0], pt1[1], pt2[0], pt2[1])

    image_bb.save(file_path)
    return