'''
@Description: Module for training options.
@Developed by: Alex Choi
@Date: Aug. 03, 2021
@Contact: alex.choi@cognex.com
'''

#%% Importing libraries
import torch
import torch.nn as nn
from torchvision.models import detection

#%% Options

model_options = {
    "fasterrcnn_resnet50_fpn": detection.fasterrcnn_resnet50_fpn,
    "fasterrcnn_mobilenet_v3_large_320_fpn": detection.fasterrcnn_mobilenet_v3_large_320_fpn,
    "fasterrcnn_mobilenet_v3_large_fpn": detection.fasterrcnn_mobilenet_v3_large_fpn
}

loss_options = {
    "CrossEntropyLoss": nn.CrossEntropyLoss(),
    "NLLLoss": nn.NLLLoss()
}

optimizer_options = {
    "Adam": torch.optim.Adam,
    "AdamW": torch.optim.AdamW,
    "RMSprop": torch.optim.RMSprop,
    "SGD": torch.optim.SGD,
    "ASGD": torch.optim.ASGD,
    "Adadelta": torch.optim.Adadelta,
    "Adagrad": torch.optim.Adagrad
}