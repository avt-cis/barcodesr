'''
@Description: The code is for creating a COCO-like data format file from PASCAL VOC files.
@Developed by: Alex Choi
@Date: Aug. 09, 2021
@Contact: alex.choi@cognex.com
'''

#%% Importing libraries
import os
import json
import random
import xml.etree.ElementTree as ET


#%% Configurations
configs = {
    "data_annot_path": "../../Dataset/HR/annot/",
    "output_path": "../annot/annotations.json",
    "train_ratio": 0.6,
    "valid_ratio": 0.3
}

# test_ratio is automatically calculated: 1.0 - (train_ratio + valid_ratio)


#%% Splitting into train / validation / test datasets
file_list = os.listdir(configs['data_annot_path'])
file_list = [filename for filename in file_list if os.path.splitext(filename)[1] == ".xml"]
ids = [id for id, _ in enumerate(file_list)]

num_train_set = int(configs["train_ratio"] * len(ids))
num_valid_set = int(configs["valid_ratio"] * len(ids))

train_ids = random.sample(ids, num_train_set)
remain_ids = set(ids).difference(train_ids)
valid_ids = random.sample(remain_ids, num_valid_set)
test_ids = set(remain_ids).difference(valid_ids)


#%% Defining data schema
info = {
    "description": "Barcode Dataset",
    "version": "1.0",
    "year": 2021,
    "contributor": "",
    "date_created": "2021/08/09"
}

categories = [
    {
        "id": 0,
        "supercategory": "",
        "name": "background"
    },
    {
        "id": 1,
        "supercategory": "",
        "name": "barcode"
    }
]

json_data = {
    "info": info,
    "categories": categories,
    "train": {
        "images": [],
        "annotations": []
    },
    "valid": {
        "images": [],
        "annotations": []
    },
    "test": {
        "images": [],
        "annotations": []
    }
}


#%% function to record COCO-like formatted JSON data from PASCAL VOC format
def record_json_data(
    annot_start_id: int,
    mode: str='train'
) -> int:

    """ Records annotations data

        Params
        --------
            annot_start_id (int): Starting index of annotations
            mode (str): train / valid / test

        Returns
        --------
            annot_id (int): End index of annotations

        """

    print(f"Started recording {mode} data....")

    ids = []
    if mode == 'train':
        ids = train_ids
    elif mode == 'valid':
        ids = valid_ids
    elif mode == 'test':
        ids = test_ids
    else:
        raise ValueError("Invalid mode name")

    annot_id = annot_start_id
    for image_id in ids:
        filename = file_list[image_id]
        file_path = os.path.join(configs['data_annot_path'], filename)

        tree = ET.parse(file_path)
        root = tree.getroot()

        width, height, depth = 0, 0, 0
        for child in root:
            if child.tag == 'path':
                image_path = child.text

            if child.tag == 'size':
                for elem in child:
                    if elem.tag == 'width': width = int(elem.text)
                    if elem.tag == 'height': height = int(elem.text)
                    if elem.tag == 'depth': depth = int(elem.text)

                json_data[mode]['images'].append({
                    "id": image_id,
                    "width": width,
                    "height": height,
                    "depth": depth,
                    "file_path": image_path
                })

            xmin, ymin, xmax, ymax = 0, 0, 0, 0
            if child.tag == 'object':
                for elem in child:
                    if elem.tag == 'bndbox':
                        for item in elem:
                            if item.tag == 'xmin': xmin = int(item.text)
                            if item.tag == 'ymin': ymin = int(item.text)
                            if item.tag == 'xmax': xmax = int(item.text)
                            if item.tag == 'ymax': ymax = int(item.text)

                        bbox = [xmin, ymin, xmax, ymax]
                        area = (xmax - xmin) * (ymax - ymin)

                        json_data[mode]['annotations'].append({
                            "id": annot_id,
                            "image_id": image_id,
                            "category_id": 1,
                            "iscrowd": 0,
                            "area": area,
                            "bbox": bbox
                        })

                        annot_id += 1

    print(f"Finished recording {mode} data.\n")
    return annot_id


#%% Main
if __name__ == "__main__":
    ## Recording the train data
    annot_id = record_json_data(annot_start_id=1, mode='train')

    ## Recording the validation data
    annot_id = record_json_data(annot_start_id=annot_id, mode='valid')

    ## Recording the test data
    annot_id = record_json_data(annot_start_id=annot_id, mode='test')

    ## JSON data dump
    with open(configs["output_path"], 'w', encoding='utf-8') as f:
        json.dump(json_data, f, ensure_ascii=False, indent=4)