'''
@Description: The code is for creating a COCO-like data format file from mask images.
@Developed by: Alex Choi
@Date: Aug. 02, 2021
@Contact: alex.choi@cognex.com
'''

#%% Importing libraries
import os
import numpy as np
import json
import random
from PIL import Image


#%% Configurations
configs = {
    "data_root_path": "D:/Projects/DeepLearning/Dataset/Barcode-SuperRes/Objection-Detection/",
    "image_path": "image",
    "mask_path": "mask",
    "mask_suffix": "_label",
    "output_path": "../annot/coco-annot.json",
    "train_ratio": 0.6,
    "valid_ratio": 0.3
}


#%% Splitting dataset
image_list = os.listdir(os.path.join(configs["data_root_path"], configs["image_path"]))
ids = [id for id, _ in enumerate(image_list)]

num_train_set = int(configs["train_ratio"] * len(ids))
num_valid_set = int(configs["valid_ratio"] * len(ids))

train_ids = random.sample(ids, num_train_set)
remain_ids = set(ids).difference(train_ids)
valid_ids = random.sample(remain_ids, num_valid_set)
test_ids = set(remain_ids).difference(valid_ids)


#%% Defining data schema
info = {
    "description": "Barcode Dataset",
    "version": "1.0",
    "year": 2021,
    "contributor": "",
    "date_created": "2021/08/01"
}

categories = [
    {
        "id": 0,
        "supercategory": "",
        "name": "background"
    },
    {
        "id": 1,
        "supercategory": "",
        "name": "barcode"
    }
]

json_data = {
    "info": info,
    "categories": categories,
    "train": {
        "images": [],
        "annotations": []
    },
    "valid": {
        "images": [],
        "annotations": []
    },
    "test": {
        "images": [],
        "annotations": []
    }
}


#%% Root paths
image_root = os.path.join(configs["data_root_path"], configs["image_path"])
mask_root = os.path.join(configs["data_root_path"], configs["mask_path"])

#%% Train set
annot_id = 0
for id in train_ids:
    image_name = image_list[id]
    image_path = os.path.join(image_root, image_name)
    mask_name = os.path.splitext(image_name)[0] + configs["mask_suffix"] + os.path.splitext(image_name)[1]
    mask_path = os.path.join(mask_root, mask_name)

    if not os.path.exists(image_path):
        raise ValueError('Not valid image path!')

    if not os.path.exists(mask_path):
        raise ValueError('Not valid mask path!')

    img = Image.open(image_path).convert("RGB")
    width = img.size[0]
    height = img.size[1]

    image_json = {
        "id": id,
        "width": width,
        "height": height,
        "file_name": image_name
    }

    json_data["train"]["images"].append(image_json)

    mask = Image.open(mask_path)
    mask = np.array(mask)

    # instances are encoded as different colors
    obj_ids = np.unique(mask)

    # first id is the background, so remove it
    obj_ids = obj_ids[1:]

    # split the color-encoded mask into a set of binary masks
    masks = mask == obj_ids[:, None, None]

    # get bounding box coordinates for each mask
    num_objs = len(obj_ids)

    for i in range(num_objs):
        annot_id += 1

        pos = np.where(masks[i])
        xmin = int(np.min(pos[1]))
        xmax = int(np.max(pos[1]))
        ymin = int(np.min(pos[0]))
        ymax = int(np.max(pos[0]))
        box = [xmin, ymin, xmax, ymax]

        area = (box[3] - box[1]) * (box[2] - box[0])

        # there is only one class now (change if multiple classes)
        label = 1

        # suppose all instances are not crowd
        iscrowd = 0

        annotations_json = {
            "id": annot_id,
            "image_id": id,
            "category_id": 1,
            "iscrowd": iscrowd,
            "area": area,
            "bbox": box
        }

        json_data["train"]["annotations"].append(annotations_json)


#%% Validation set
for id in valid_ids:
    image_name = image_list[id]
    image_path = os.path.join(image_root, image_name)
    mask_name = os.path.splitext(image_name)[0] + configs["mask_suffix"] + os.path.splitext(image_name)[1]
    mask_path = os.path.join(mask_root, mask_name)

    if not os.path.exists(image_path):
        raise ValueError('Not valid image path!')

    if not os.path.exists(mask_path):
        raise ValueError('Not valid mask path!')

    img = Image.open(image_path).convert("RGB")
    width = img.size[0]
    height = img.size[1]

    image_json = {
        "id": id,
        "width": width,
        "height": height,
        "file_name": image_name
    }

    json_data["valid"]["images"].append(image_json)

    mask = Image.open(mask_path)
    mask = np.array(mask)

    # instances are encoded as different colors
    obj_ids = np.unique(mask)

    # first id is the background, so remove it
    obj_ids = obj_ids[1:]

    # split the color-encoded mask into a set of binary masks
    masks = mask == obj_ids[:, None, None]

    # get bounding box coordinates for each mask
    num_objs = len(obj_ids)

    for i in range(num_objs):
        annot_id += 1

        pos = np.where(masks[i])
        xmin = int(np.min(pos[1]))
        xmax = int(np.max(pos[1]))
        ymin = int(np.min(pos[0]))
        ymax = int(np.max(pos[0]))
        box = [xmin, ymin, xmax, ymax]

        area = (box[3] - box[1]) * (box[2] - box[0])

        # there is only one class now (change if multiple classes)
        label = 1

        # suppose all instances are not crowd
        iscrowd = 0

        annotations_json = {
            "id": annot_id,
            "image_id": id,
            "category_id": 1,
            "iscrowd": iscrowd,
            "area": area,
            "bbox": box
        }

        json_data["valid"]["annotations"].append(annotations_json)


#%% Test set
for id in test_ids:
    image_name = image_list[id]
    image_path = os.path.join(image_root, image_name)
    mask_name = os.path.splitext(image_name)[0] + configs["mask_suffix"] + os.path.splitext(image_name)[1]
    mask_path = os.path.join(mask_root, mask_name)

    if not os.path.exists(image_path):
        raise ValueError('Not valid image path!')

    if not os.path.exists(mask_path):
        raise ValueError('Not valid mask path!')

    img = Image.open(image_path).convert("RGB")
    width = img.size[0]
    height = img.size[1]

    image_json = {
        "id": id,
        "width": width,
        "height": height,
        "file_name": image_name
    }

    json_data["test"]["images"].append(image_json)

    mask = Image.open(mask_path)
    mask = np.array(mask)

    # instances are encoded as different colors
    obj_ids = np.unique(mask)

    # first id is the background, so remove it
    obj_ids = obj_ids[1:]

    # split the color-encoded mask into a set of binary masks
    masks = mask == obj_ids[:, None, None]

    # get bounding box coordinates for each mask
    num_objs = len(obj_ids)

    for i in range(num_objs):
        annot_id += 1

        pos = np.where(masks[i])
        xmin = int(np.min(pos[1]))
        xmax = int(np.max(pos[1]))
        ymin = int(np.min(pos[0]))
        ymax = int(np.max(pos[0]))
        box = [xmin, ymin, xmax, ymax]

        area = (box[3] - box[1]) * (box[2] - box[0])

        # there is only one class now (change if multiple classes)
        label = 1

        # suppose all instances are not crowd
        iscrowd = 0

        annotations_json = {
            "id": annot_id,
            "image_id": id,
            "category_id": 1,
            "iscrowd": iscrowd,
            "area": area,
            "bbox": box
        }

        json_data["test"]["annotations"].append(annotations_json)


#%% JSON data dump
with open(configs["output_path"], 'w', encoding='utf-8') as f:
    json.dump(json_data, f, ensure_ascii=False, indent=4)