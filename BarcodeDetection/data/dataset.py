'''
@Description: Module for custom dataset.
@Developed by: Alex Choi
@Date: Aug. 02, 2021
@Contact: alex.choi@cognex.com
'''

#%% Importing libraries
import json
import os

from logger import logging
from PIL import Image
import torch
import torchvision
from torch.utils.data import Dataset
from torchvision import transforms as T


#%% Class for Custom Dataset
class CustomDataset(Dataset):

    """ Class to define a custom dataset

        Params
        --------
            annot_path (str): Path to JSON annotation file
            mode (str): Mode option (train / valid / test)
            transforms (torchvision.transforms): Image transform

    """

    def __init__(
        self,
        annot_path: str,
        mode: str='train',
        transforms:torchvision.transforms=None
    ):
        self.mode = mode
        self.transforms = transforms

        with open(annot_path) as json_file:
            annot_data = json.load(json_file)

        if mode == 'train':
            mode_data = annot_data["train"]
        elif mode == 'valid':
            mode_data = annot_data["valid"]
        elif mode == 'test':
            mode_data = annot_data["test"]
        else:
            logging.error("Invalid mode. Please select: 'train' / 'valid' / 'test'")
            return

        self.imgs = mode_data['images']
        self.annots = mode_data["annotations"]
        self.num_classes = len(annot_data['categories'])

    def __getitem__(self, idx):
        image = self.imgs[idx]
        image_id = image['id']
        file_path = image['file_path']
        img = Image.open(file_path).convert("RGB")

        annotations = []
        for annot in self.annots:
            if annot['image_id'] == image_id:
                annotations.append(annot)

        num_objs = len(annotations)
        boxes = []
        area = []
        for annotation in annotations:
            boxes.append(annotation['bbox'])
            area.append(annotation['area'])

        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        area = torch.as_tensor(area, dtype=torch.float32)
        labels = torch.ones((num_objs,), dtype=torch.int64)
        image_id = torch.tensor([image_id])
        iscrowd = torch.zeros((num_objs,), dtype=torch.int64)

        target = {}
        target["boxes"] = boxes
        target["labels"] = labels
        target["image_id"] = image_id
        target["area"] = area
        target["iscrowd"] = iscrowd

        if self.transforms is not None:
            img, target = self.transforms(img, target)

        return img, target

    def __len__(self):
        return len(self.imgs)


#%% Class for Dataset from a folder
class DatasetFromFolder(Dataset):

    """ Class to define a custom dataset from a folder

        Params
        --------
            folder_path (str): Path to JSON annotation file

    """

    def __init__(self, folder_path: str):
        file_list = [os.path.join(folder_path, filename) for filename in os.listdir(folder_path)]
        self.imgs = file_list

        transforms = []
        transforms.append(T.ToTensor())
        self.transform = T.Compose(transforms)

    def __getitem__(self, idx):
        image_path = self.imgs[idx]
        img = Image.open(image_path).convert("RGB")
        img = self.transform(img)

        return img

    def __len__(self):
        return len(self.imgs)