'''
@Description: Module for custom dataset.
@Developed by: Alex Choi
@Date: Aug. 02, 2021
@Contact: alex.choi@cognex.com
'''

#%% Importing Libraries
import torch
from torch.utils.data import DataLoader

from .dataset import CustomDataset, DatasetFromFolder


#%% function: get_data_loader
def get_data_loader(
    dataset: torch.utils.data.dataset,
    batch_size: int,
    shuffle: bool,
    num_workers: int,
    pin_memory: bool,
    collate_fn
) -> torch.utils.data.DataLoader:

    """ Gets data loader from the dataset

        Params
        --------
            dataset (torch.utils.data.dataset): dataset
            batch_size (int): Batch size
            shuffle (bool): Option for data shuffling
            num_workers (int): Number of threads for data processing
            pin_memory (bool): Option to use pin memory
            collate_fn: Collate function

        Returns
        --------
            data_loader (torch.utils.data.DataLoader): Dataloader

    """

    data_loader = DataLoader(
        dataset,
        batch_size=batch_size,
        shuffle=shuffle,
        num_workers=num_workers,
        pin_memory=pin_memory,
        collate_fn=collate_fn
    )

    return data_loader