# Barcode Image Super-resolution

* Document Version: 0.6
* Last Date: 08/23/2021
* Author: Alex Choi (alex.choi@cognex.com)

**_NOTE_**

Before starting please set up the development environment.

## Resources
* [Barcode image dataset](https://cognexcorporation-my.sharepoint.com/:u:/g/personal/alex_choi_cognex_com/Efv0whK8DmtGuBD0wFqPCMMBSI6vjIhVgTrZfPvl2sqkOA?e=WvxxLA)

Please download the barcode image dataset to the project root path.

The folder structure of the project root may look like below.

![folder structure](./images/folder_structure.PNG)

## Development Environment

#### 1. Set Up `Python Virtual Environment`

This `Barcode Image Super-resolution` is developed on Python 3.8.

However, you can also try with other versions of Python at your own risk.

I would assume you are using Python 3.8 for the purpose of explanation.

First of all, visit [Python 3.8 download site](https://www.python.org/downloads/release/python-380/) to download it.

After installing Python, install a Python virtual environment:

```
py -m pip install --user virtualenv
python -m pip install --upgrade pip
```

Now create a virtual environment with your environment name, for example, `barcode_dl`:

```
python -m venv barcode_sr
```

Now activate activate the virtual environment just created:

```
.\barcode_sr\Scripts\activate
```

#### 2. Install the Python Packages

Move to your barcode project root path that you cloned from [the stash repository](https://stash-server.pc.cognex.com:8443/projects/BS/repos/barcode-super-resolution/browse), where you can find `requirements.txt` file, to install all the Python dependencies:

```
pip install -r requirements.txt
```

It takes some time to finish installing, please take your time. :)

## Barcode Detection

Here an deep learning based object detection algorithm, [Faster R-CNN](https://arxiv.org/abs/1506.01497), is used to detect the barcode regions.

Despite `Faster R-CNN` is relatively faster than other R-CNN type algorithms, i.e. R-CNN, Fast R-CNN, it's slower than [YOLO](https://pjreddie.com/darknet/yolo/) or [Single Shot Detector](https://arxiv.org/abs/1512.02325)(SSD), which sacrifice the detection performance a little bit while getting faster processing speed.

![detection_fps](./images/detection_fps.png)

If you are interested in latest version of YOLO ([YOLO v5](https://github.com/ultralytics/yolov5)), please download the code:

* [Download YOLO V5 Code](https://drive.google.com/drive/folders/1s_hnazk71WajgQ9oI-iOkrmIpuWNOI9u?usp=sharing)


#### 1. Barcode Region Labeling

An open source free labeling tool, [`LabelImg`](https://github.com/tzutalin/labelImg), is used in this project to create a COCO-like labeling format that includes the bounding boxes information.

Type the following command in your CLI tool in your Python Virtual Environment:

```
labelImg
```

Then `labelImg` tool will show up, and select the image directory, i.e. `{PROJECT_ROOT}/Dataset/HR/images/`, to label by clicking `Open Dir` button.

![labelImg-01.png](./images/labelImg-01.png)

Click `Creact RectBox` or press `w` for keyboard shortcut to create a bounding box named `barcode` or any name you want.

![labelImg-02.png](./images/labelImg-02.png)

Once you created the bounding box on the current image, please make sure to save the label file(.xml) with [`PascalVOC`](https://towardsdatascience.com/coco-data-format-for-object-detection-a4c5eaf518c5) is selected as an exporting format.

If you didn't change the save directory by clicking `Change Save Dir`, `labelImg` will save the label file in your image directory.

So it is recommended to create any annotation directory to save the label files.

Once you finished labeling for all of the images, you will find the exact same number of Pascal VOC files (.xml) as that of images in the specified `Save Dir`.

One example of your Pascal VOC file may look like the following.

```
<annotation verified="yes">
	<folder>images</folder>
	<filename>code128_001_ppm(1.08).bmp</filename>
	<path>D:\Projects\DeepLearning\Barcode-SR-Stash\barcode-super-resolution\Dataset\HR\images\code128_001_ppm(1.08).bmp</path>
	<source>
		<database>Unknown</database>
	</source>
	<size>
		<width>1600</width>
		<height>1200</height>
		<depth>1</depth>
	</size>
	<segmented>0</segmented>
	<object>
		<name>barcode</name>
		<pose>Unspecified</pose>
		<truncated>0</truncated>
		<difficult>0</difficult>
		<bndbox>
			<xmin>695</xmin>
			<ymin>618</ymin>
			<xmax>832</xmax>
			<ymax>707</ymax>
		</bndbox>
	</object>
</annotation>
```

#### 2. Converting to COCO-like Format File

Since our `Barcode Detection` code receives a `COCO-like format` as an input.

The meaning of `COCO-like format` is the format is slightly different from the standard COCO format.

Regarding the standard COCO format, please refer to [this](https://docs.aws.amazon.com/ko_kr/rekognition/latest/customlabels-dg/cd-coco-overview.html).

In order to convert from several Pascal VOC format files (`.xml`) to one COCO-like format file (`.json`), you can use `{PROJECT_ROOT}/BarcodeDetection/utilities/json-data-maker-from-pascal.py`.

Open the file up in your Python IDE, and modify `configs` variable according to your environment, for example,

```
configs = {
    "data_annot_path": "../Dataset/HR/annot/",
    "output_path": "../annot/annotations.json",
    "train_ratio": 0.6,
    "valid_ratio": 0.3
}
```

* `data_annot_path`: path to the annotation data files (Pascal VOC files)
* `output_path`: path to save the output COCO-like format file
* `train_ratio`: ratio of data to be used for training
* `valid_ratio`: ratio of data to be used for validation

**_NOTE_**

Test ratio is automatically calculated by `test_ratio = 1.0 - (train_ratio + valid_ratio)`.

After running `json-data-maker-from-pascal.py`, you will get one `.json` annotation file, which may look like:

```
{
    "info": {
        "description": "Barcode Dataset",
        "version": "1.0",
        "year": 2021,
        "contributor": "",
        "date_created": "2021/08/19"
    },
    "categories": [
        {
            "id": 0,
            "supercategory": "",
            "name": "background"
        },
        {
            "id": 1,
            "supercategory": "",
            "name": "barcode"
        }
    ],
    "train": {
        "images": [
            {
                "id": 6,
                "width": 1024,
                "height": 768,
                "depth": 1,
                "file_path": "D:\\Projects\\DeepLearning\\Barcode-SR-Stash\\barcode-super-resolution\\Dataset\\HR\\images\\code128_1Z6586290260820020_1.6ppm.bmp"
            },

            ...

            {
                "id": 4,
                "width": 1600,
                "height": 1200,
                "depth": 1,
                "file_path": "D:\\Projects\\DeepLearning\\Barcode-SR-Stash\\barcode-super-resolution\\Dataset\\HR\\images\\code128_005_ppm(0.91).bmp"
            }
        ],
        "annotations": [
            {
                "id": 1,
                "image_id": 6,
                "category_id": 1,
                "iscrowd": 0,
                "area": 62425,
                "bbox": [
                    441,
                    59,
                    716,
                    286
                ]
            },

            ...

            {
                "id": 7,
                "image_id": 4,
                "category_id": 1,
                "iscrowd": 0,
                "area": 27738,
                "bbox": [
                    752,
                    530,
                    953,
                    668
                ]
            }
        ]
    },
    "valid": {
        "images": [
            {
                "id": 2,
                "width": 1600,
                "height": 1200,
                "depth": 1,
                "file_path": "D:\\Projects\\DeepLearning\\Barcode-SR-Stash\\barcode-super-resolution\\Dataset\\HR\\images\\code128_003_ppm(1.08).bmp"
            },

            ...

            {
                "id": 10,
                "width": 1024,
                "height": 768,
                "depth": 1,
                "file_path": "D:\\Projects\\DeepLearning\\Barcode-SR-Stash\\barcode-super-resolution\\Dataset\\HR\\images\\code128_1ZE6E4990260089882_1.6ppm.bmp"
            }
        ],
        "annotations": [
            {
                "id": 8,
                "image_id": 2,
                "category_id": 1,
                "iscrowd": 0,
                "area": 13566,
                "bbox": [
                    532,
                    720,
                    634,
                    853
                ]
            },

            ...

            {
                "id": 11,
                "image_id": 10,
                "category_id": 1,
                "iscrowd": 0,
                "area": 52136,
                "bbox": [
                    806,
                    66,
                    939,
                    458
                ]
            }
        ]
    },
    "test": {
        "images": [
            {
                "id": 5,
                "width": 1600,
                "height": 1200,
                "depth": 1,
                "file_path": "D:\\Projects\\DeepLearning\\Barcode-SR-Stash\\barcode-super-resolution\\Dataset\\HR\\images\\code128_006_ppm(0.90).bmp"
            }
        ],
        "annotations": [
            {
                "id": 12,
                "image_id": 5,
                "category_id": 1,
                "iscrowd": 0,
                "area": 30090,
                "bbox": [
                    517,
                    359,
                    687,
                    536
                ]
            }
        ]
    }
}
```

#### 3. Configuring Training Options

In `{PROJECT_ROOT}/BarcodeDetection/config/configs.yml`, you can easily set up your barcode detection training configurations.

Here are the descriptions on each option:

* `model_name`: name of your barcode detection model. any name would be fine.
* `device`: `cpu` or `gpu`. if `gpu`, then computing will be running on `cuda`.
* `data`:
  * `annot_json_path`: path to annotation `.json` format file (in COCO-like format)
  * `shuffle`:
    * `train`: option to use shuffling for train dataset.
    * `valid`: option to use shuffling for validation dataset.
    * `test`: option to use shuffling for test dataset.
  * `data_pipeline`:
    * `num_workers`: number of threads for data processing. if you are using `nt` OS (Windows), automatically set to `0` because `num_workers` > 0 is not supported.
    * `pin_memory`: option to use [pin memory](https://pytorch.org/docs/stable/data.html#memory-pinning).
* `mode`: mode to run
	- `train`
	- `valid`
	- `test`
* `train`: options for training
  * `pretrained_model`: option for pretrained models
    - `fasterrcnn_resnet50_fpn`
    - `fasterrcnn_mobilenet_v3_large_320_fpn`
    - `fasterrcnn_mobilenet_v3_large_fpn`
  * `epochs`: number of epochs to train
	* `early_stop_patience`: number of no improvements for early stopping. if no improvements of validation loss within the specified `early_stop_patience`. then stop the training
	* `batch_size`: batch size of train dataset
	* `optimizer`: optimization algorithms
		- `Adam`
		- `AdamW`
		- `RMSprop`
		- `SGD`
		- `ASGD`
		- `Adadelta`
		- `Adagrad`
	* `optim_criterion`: opimitzation criterion (loss function)
		- `CrossEntropyLoss`
		- `NLLLoss`
	* `print_every`: frequency of printing the results (loss, metrics, etc.)
	* `lr_scheduler`: learning rate scheduler
		* `algorithm`: learning rate scheduler algorithm
			- `StepLR`: for more details of this learning rate scheduler algorithm, please refer to [here](https://pytorch.org/docs/stable/generated/torch.optim.lr_scheduler.StepLR.html)
			- `ReduceLROnPlateau`: for more details of this learning rate scheduler algorithm, please refer to [here](https://pytorch.org/docs/stable/generated/torch.optim.lr_scheduler.ReduceLROnPlateau.html)
* `valid`: options for validation
	* `batch_size`: batch size of validation dataset
* `test`: options for test
	* `mode`: test mode
		* `normal`: test without data augmentations
		* `tta`: [test time augmentation](https://machinelearningmastery.com/how-to-use-test-time-augmentation-to-improve-model-performance-for-image-classification/) (not available now)
	* `model_path`: path to the model to test
	* `image_folder`: path to the test images
	* `test_output_path`: path to the outputs of testing
	* `batch_size`: batch size of test dataset
	* `threshold`: threshold for prediction score
	* `bbox_pixel_margin`: margin in pixels for the bounding box cropping


## Barcode Super-resolution

#### 1. Preparation of Dataset

You don't have to prepare low resolution images. Only prepare high resolution images, and low resolution images are automatically provided by PyTorch transform functions.

In order to get the understanding of how low resolution images are obtained, please refer to `{PROJECT_ROOT}/EDSR/data/dataset.py`.

In `CustomDataset` class in `dataset.py`, you can find some resize interpolation options like,

```
self.interpolation_options = [
    F.InterpolationMode.BICUBIC,
    F.InterpolationMode.BILINEAR,
    F.InterpolationMode.HAMMING,
    F.InterpolationMode.LANCZOS,
    F.InterpolationMode.NEAREST,
    F.InterpolationMode.BOX
]
```

In `__getitem__()` method, image transform for low resolution is randomly chosen from the resize interpolation options above.

```
lr_transforms = transforms.Compose([
    transforms.ToPILImage(),
    transforms.Grayscale(num_output_channels=1),
    transforms.Resize(
        (self.image_size // self.upscale_factor, self.image_size // self.upscale_factor),
        interpolation=random.choice(self.interpolation_options)
    ),
    transforms.ToTensor()
])
```

You can add more resize options as you want, or you can make the option deterministic by replacing `random.choice()` by any specified resize algorithm.

#### 2. Network Structure

In order to understand the network structure, please refer to the code, `{PROJECT_ROOT}/EDSR/models/__init__.py`.

##### _ResNet Block_

The main idea of EDSR is based on [SRResNet](https://arxiv.org/abs/1609.04802), but Batch Normalization (BN) is removed because the BN layers normalize the features, and get rid of range flexibility which is better to remove them. In addition, the GPU memory usage is also sufficiently reduced (approx. 40% of memory usage compared to SRResNet).

![ResNet Block](./images/resnet_block.png)

The structure of EDSR network is shown as below.

![edsr_network_strcture.png](./images/edsr_network_strcture.png)

In the basic structure of EDSR the network size is determined by the number of ResNet blocks (_B_) and the number of feature channels in each ResNet block (_F_).

In the original paper, _B_ = 32 and _F_ = 256, but this size of network requires quite huge amount of GPU memory (You may see `CUDA Out of Memory` error message depending on your GPU card).

Thus please carefully choose those parameters so that your GPU card can afford it - GPU memory usage is also related to the batch size.

##### _ResNet Block Code Implementation_

![resnet_block_2.png](./images/resnet_block_2.png)

The green dotted box shows the components in a ResNet block.

The code below is nothing but the PyTorch code implementation for the ResNet block.

![resnet_block_code.png](./images/resnet_block_code.png)

The authors proposed Residual Scaling (`res_scale`) as a hyperparameter, and they claim that this factor relieves numerically unstable issue in the training procedure.

##### _EDSR - head & body_

`head` component is just a `Conv2D` block, and `body` is composed of a series of ResNet blocks (`ResBlock`) and a `Conv2D` block at the end.

![edsr_01](./images/edsr_01.png)

The corresponding code implementations is as shown below.

![edsr_head_body.png](./images/edsr_head_body.png)

##### _EDSR - upscale_

The dotted green bounding box shows `Upscale` block. `Upscale` block is composed of `Conv2D` and `PixelShuffle` layers. `x2` and `x3` upscalings are the same, but `x4` upscale has one more repetitive `Conv2D` and `PixelShuffle` layers.

![edsr_upscale.png](./images/edsr_upscale.png)

The corresponding code implementations is as shown below.

![edsr_upscale_code.png](./images/edsr_upscale_code.png)

##### _EDSR - tail_

`tail` is nothing but a `Conv2D` layer at the end of the EDSR structure.

![edsr_tail.png](./images/edsr_tail.png)

The corresponding code implementations is as shown below.

![edsr_tail_code.png](./images/edsr_tail_code.png)