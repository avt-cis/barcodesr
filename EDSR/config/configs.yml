# ---------- OPTIONS ----------
# data:
#    train_path:                    path to train image dataset
#    valid_path:                    path to validation image dataset
#
# device:
#   gpu:                            computing with cuda
#   cpu:                            computing with cpu
#
# data_pipeline:
#    num_workers:                   number of thread for data processing (only 0 is acceptable for Windows)
#    pin_memory:                    option to use pin memory
#
# model:
#    scale:                         scale for super-resolution (2, 3, 4, ...)
#    num_channels:                  number of channels (1 for grayscale or 3 for rgb)
#    num_feats:                     number of features in EDSR network
#    num_blocks:                    number of residual blocks
#    res_scale:                     scale factor for residual block
#    patch_size:                    image crop size for network input
#
# train:
#    from:                          train option (scratch: train from scratch / checkpoint: train from specified checkpoint)
#    checkpoint:                    checkpoint path if train from 'checkpoint'
#    psnr_lr:                       learning rate of the optimizer
#    batch_size:                    train batch size
#    num_epochs:                    number of epochs
#    seed:                          torch seed for data shuffling
#    shuffle:                       data shuffling option
#
# valid:
#    batch_size:                    validation batch size
#    shuffle:                       data shuffling option
#
# test:
#    example_image_path:            path to an example image to test during training
#    model_path:                    path to the trained model to test
#    batch_size:                    test batch size
#    shuffle:                       data shuffling option
#
# outputs:
#    root_path:                     path to the root of outputs
#    checkpoint_file_path:          path to the checkpoint files to save
#    path_to_save_test_example:     path to save the test result images
#    path_to_save_eval_examples:    path to save the evaluation result images
# -----------------------------

data:
    train_path: ../Dataset/HR/images
    valid_path: ../Dataset/HR/images

device: gpu

data_pipeline:
    num_workers: 0
    pin_memory: True

model:
    scale: 2
    num_channels: 1
    num_feats: 128
    num_blocks: 20
    res_scale: 0.1
    patch_size: 368

train:
    from: checkpoint
    checkpoint: ./outputs/EDSR_x2/epoch_92.pth
    psnr_lr: 1e-4
    batch_size: 16
    num_epochs: 200
    seed: 123
    shuffle: True

valid:
    batch_size: 8
    shuffle: False

test:
    example_image_path: ../Dataset/CroppedHR/code128_42001742_1.8ppm_rot3_cropped-0.bmp
    model_path: ./outputs/best-1.model
    batch_size: 1
    shuffle: False

eval:
    eval_image_path: ../Dataset/CroppedHR/

outputs:
    root_path: ./outputs
    checkpoint_file_path: ./outputs/checkpoint/checkpoint-file.pth
    path_to_save_test_example: ./outputs/Test/
    path_to_save_eval_examples: ./outputs/Eval/