'''
@Description: Module for custom dataset.
@Developed by: Alex Choi
@Date: Aug. 09, 2021
@Contact: alex.choi@cognex.com
'''

from .dataset import CustomDataset