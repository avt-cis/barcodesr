'''
@Description: Tester of Barcode Super-Resolution (only one image)
@Developed by: Alex Choi
@Date: Aug. 11, 2021
@Contact: alex.choi@cognex.com
'''


#%% Importing libraries
import os
import torch.backends.cudnn as cudnn
from utils import (
    get_configurations,
    get_device,
    get_trained_model,
    test_one_image
)


#%% Configurations
YAML_CONFIG_PATH = "config/configs.yml"


#%% function: main
def main():
    ## Getting parameters
    configs = get_configurations(YAML_CONFIG_PATH)

    ## Creating the output path to save the test results
    path_to_save_test_example = configs['outputs']['path_to_save_test_example']
    if not os.path.exists(path_to_save_test_example):
        os.mkdir(path_to_save_test_example)

    ## Extracting filename and extension
    filename = configs['test']['example_image_path'].split('/')[-1]
    filename_only = os.path.splitext(filename)[0]
    filename_ext = os.path.splitext(filename)[1]

    ## Setting computing device
    cudnn.benchmark = True
    device = get_device(compute_device=configs['device'])

    ## Loading the trained model
    model = get_trained_model(
        model_path=configs['test']['model_path'],
        scale_factor=configs['model']['scale'],
        num_channels=configs['model']['num_channels'],
        num_feats=configs['model']['num_feats'],
        num_blocks=configs['model']['num_blocks'],
        res_scale=configs['model']['res_scale'],
        device=device
    )

    path_to_save_interpolation = os.path.join(
        configs['outputs']['path_to_save_test_example'],
        f"{filename_only}_bicubic-{configs['model']['scale']}x{filename_ext}"
    )

    path_to_save_edsr = os.path.join(
        configs['outputs']['path_to_save_test_example'],
        f"{filename_only}_edsr-{configs['model']['scale']}x{filename_ext}"
    )

    test_one_image(
        test_image_path=configs['test']['example_image_path'],
        num_channels=configs['model']['num_channels'],
        scale=configs['model']['scale'],
        model=model,
        interpolation_save_path=path_to_save_interpolation,
        edsr_save_path=path_to_save_edsr,
        device=device
    )

    return


#%% Main
if __name__ == '__main__':
    main()