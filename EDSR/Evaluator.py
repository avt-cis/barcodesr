'''
@Description: Evaluator of Barcode Super-Resolution (images from folder)
@Developed by: Alex Choi
@Date: Aug. 12, 2021
@Contact: alex.choi@cognex.com
'''


#%% Importing libraries
import os
from logger import logging
import torch.backends.cudnn as cudnn
from utils import (
    get_configurations,
    get_device,
    get_trained_model,
    test_one_image
)


#%% Configurations
YAML_CONFIG_PATH = "config/configs.yml"


#%% function: main
def main():
    ## Getting parameters
    configs = get_configurations(YAML_CONFIG_PATH)

    ## Creating the output path to save the test results
    path_to_save_eval_examples = configs['outputs']['path_to_save_eval_examples']
    if not os.path.exists(path_to_save_eval_examples):
        os.mkdir(path_to_save_eval_examples)

    ## Setting computing device
    cudnn.benchmark = True
    device = get_device(compute_device=configs['device'])

    ## Loading the trained model
    model = get_trained_model(
        model_path=configs['test']['model_path'],
        scale_factor=configs['model']['scale'],
        num_channels=configs['model']['num_channels'],
        num_feats=configs['model']['num_feats'],
        num_blocks=configs['model']['num_blocks'],
        res_scale=configs['model']['res_scale'],
        device=device
    )

    ## Evaluating the images
    eval_image_path = configs['eval']['eval_image_path']
    image_list = os.listdir(eval_image_path)

    for idx, image_name in enumerate(image_list):
        image_path = os.path.join(eval_image_path, image_name)

        # Extracting filename and extension
        filename_only = os.path.splitext(image_name)[0]
        filename_ext = os.path.splitext(image_name)[1]

        path_to_save_interpolation = os.path.join(
            path_to_save_eval_examples,
            f"{filename_only}_inter_{configs['model']['scale']}x{filename_ext}"
        )

        path_to_save_edsr = os.path.join(
            path_to_save_eval_examples,
            f"{filename_only}_edsr_{configs['model']['scale']}x{filename_ext}"
        )

        test_one_image(
            test_image_path=image_path,
            num_channels=configs['model']['num_channels'],
            scale=configs['model']['scale'],
            model=model,
            interpolation_save_path=path_to_save_interpolation,
            edsr_save_path=path_to_save_edsr,
            device=device
        )

        if idx % 20 == 0:
            logging.info(f"Processed: {idx + 1} / {len(image_list)}")

    return


#%% Main
if __name__ == '__main__':
    main()