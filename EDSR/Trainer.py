'''
@Description: Trainer of Barcode Super-Resolution
@Developed by: Alex Choi
@Date: Aug. 11, 2021
@Contact: alex.choi@cognex.com
'''


#%% Importing libraries
import os
from logger import logging

from models import EDSR
from data import CustomDataset
from tqdm import tqdm
from PIL import Image

import torch
import torch.backends.cudnn as cudnn
import torchvision.utils as vutils
from torch.utils.data.dataloader import DataLoader
from torch import nn
from torch.cuda import amp
from torch.optim import lr_scheduler

from utils import (
    get_configurations,
    get_device,
    AverageMeter,
    ProgressMeter,
    calc_psnr,
    preprocess
)


#%% Configurations
YAML_CONFIG_PATH = "config/configs.yml"


#%% function: main
def main():
    ## Getting parameters
    configs = get_configurations(YAML_CONFIG_PATH)

    ## Creating output directory
    outputs_dir = os.path.join(configs['outputs']['root_path'], f"EDSR_x{configs['model']['scale']}")
    if not os.path.exists(outputs_dir):
        os.makedirs(outputs_dir)

    ## Setting computing device
    cudnn.benchmark = True
    device = get_device(compute_device=configs['device'])

    ## Setting torch seed
    torch.manual_seed(configs['train']['seed'])

    ## Testing an image
    test_image_path = configs['test']['example_image_path']
    if configs['model']['num_channels'] == 1:
        test_image = Image.open(test_image_path).convert('L')
    elif configs['model']['num_channels'] == 3:
        test_image = Image.open(test_image_path).convert('RGB')
    else:
        raise ValueError("Invalid number of channels.")

    test_image = preprocess(test_image)

    ## Loading the model
    model = EDSR(
        scale_factor=configs['model']['scale'],
        num_channels=configs['model']['num_channels'],
        num_feats=configs['model']['num_feats'],
        num_blocks=configs['model']['num_blocks'],
        res_scale=configs['model']['res_scale']
    ).to(device)

    ## Loss and Optimizer
    pixel_criterion = nn.L1Loss().to(device)
    psnr_optimizer = torch.optim.Adam(
        model.parameters(),
        float(configs['train']['psnr_lr']),
        (0.9, 0.999)
    )

    total_epoch = configs['train']['num_epochs']
    start_epoch = 0
    best_psnr = 0

    ## Loading weights from an existing checkpoint file
    if configs['train']['from'] =='checkpoint':
        if os.path.exists(configs['train']['checkpoint']):
            checkpoint = torch.load(configs['train']['checkpoint'])
            model.load_state_dict(checkpoint['model_state_dict'])
            psnr_optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
            start_epoch = checkpoint['epoch'] + 1
            loss = checkpoint['loss']
            best_psnr = checkpoint['best_psnr']

    ## Printing train settings information
    logging.info(
        f"EDSR MODEL INFO:\n"
        f"\tScale:                         {configs['model']['scale']}\n"
        f"\tNumber of channels:            {configs['model']['num_channels']}\n"
        f"\tNumber of blocks:              {configs['model']['num_blocks']}\n"
        f"\tNumber of features:            {configs['model']['num_feats']}\n"
        f"\tResidual scale:                {configs['model']['res_scale']}\n"

        f"EDSR TRAINING INFO:\n"
        f"\tTotal Epoch:                   {configs['train']['num_epochs']}\n"
        f"\tStart Epoch:                   {start_epoch}\n"
        f"\tTrain directory path:          {configs['data']['train_path']}\n"
        f"\tTest directory path:           {configs['data']['valid_path']}\n"
        f"\tOutput weights directory path: {configs['outputs']['root_path']}\n"
        f"\tPSNR learning rate:            {configs['train']['psnr_lr']}\n"
        f"\tPatch size:                    {configs['model']['patch_size']}\n"
        f"\tBatch size:                    {configs['train']['batch_size']}\n"
    )

    ## Setting learning scheduler
    psnr_scheduler = lr_scheduler.StepLR(
        psnr_optimizer,
        step_size=30,
        gamma=0.1
    )
    scaler = amp.GradScaler()

    ## Setting dataset
    train_dataset = CustomDataset(
        images_dir=configs['data']['train_path'],
        image_size=configs['model']['patch_size'],
        num_channels=configs['model']['num_channels'],
        upscale_factor=configs['model']['scale']
    )
    train_dataloader = DataLoader(
        dataset=train_dataset,
        batch_size=configs['train']['batch_size'],
        shuffle=configs['train']['shuffle'],
        num_workers=configs['data_pipeline']['num_workers'],
        pin_memory=configs['data_pipeline']['pin_memory']
    )

    eval_dataset = CustomDataset(
        images_dir=configs['data']['valid_path'],
        image_size=configs['model']['patch_size'],
        num_channels=configs['model']['num_channels'],
        upscale_factor=configs['model']['scale']
    )
    eval_dataloader = DataLoader(
        dataset=eval_dataset,
        batch_size=configs['valid']['batch_size'],
        shuffle=configs['valid']['shuffle'],
        num_workers=configs['data_pipeline']['num_workers'],
        pin_memory=configs['data_pipeline']['pin_memory']
    )

    ## Start training and testing
    for epoch in tqdm(range(start_epoch, total_epoch)):
        model.train()
        losses = AverageMeter(name="PSNR Loss", fmt=":.6f")
        psnr = AverageMeter(name="PSNR", fmt=":.6f")
        progress = ProgressMeter(
            num_batches=len(train_dataloader),
            meters=[losses, psnr],
            prefix=f"Epoch: [{epoch}]"
        )

        # training epoch
        for i, (lr, hr) in enumerate(train_dataloader):
            lr = lr.to(device)
            hr = hr.to(device)

            psnr_optimizer.zero_grad()

            with amp.autocast():
                preds = model(lr)
                loss = pixel_criterion(preds, hr)

            scaler.scale(loss).backward()
            scaler.step(psnr_optimizer)
            scaler.update()

            losses.update(loss.item(), len(lr))
            psnr.update(calc_psnr(preds, hr), len(lr))

            if i % 100 == 0:
                progress.display(i)

        psnr_scheduler.step()

        # testing epoch
        model.eval()
        with torch.no_grad():
            for i, (lr, hr) in enumerate(eval_dataloader):
                lr = lr.to(device)
                hr = hr.to(device)
                preds = model(lr)

        if psnr.avg > best_psnr:
            best_psnr = psnr.avg
            torch.save(
                model.state_dict(),
                os.path.join(outputs_dir, 'best.model')
            )

        torch.save(
            {
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': psnr_optimizer.state_dict(),
                'loss': loss,
                'best_psnr': best_psnr,
            },
            os.path.join(outputs_dir, 'epoch_{}.pth'.format(epoch))
        )

        # test with an example barcode cropped image
        with torch.no_grad():
            lr = test_image.to(device)
            preds = model(lr)
            vutils.save_image(preds.detach(), os.path.join(outputs_dir, f"PSNR_{epoch}.png"))

    return


#%% Main
if __name__ == '__main__':
    main()