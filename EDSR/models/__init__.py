'''
@Description: Module for dataset.
@Developed by: Alex Choi
@Date: Aug. 09, 2021
@Contact: alex.choi@cognex.com
'''


#%% Importing libraries
import torch.nn as nn
from torch.cuda import amp


#%% Class for ResNet Block
class ResBlock(nn.Module):

    """ Defines ResNet Block

        Params
        --------
            n_feats (int):          Number of features
            res_scale (float):      Scale factor for ResNet weights

    """

    def __init__(self, n_feats: int, res_scale: float=1.0):
        super(ResBlock, self).__init__()
        m = []
        for i in range(2):
            m.append(nn.Conv2d(n_feats, n_feats, kernel_size=3, bias=True, padding=3//2))
            if i == 0:
                m.append(nn.ReLU(True))

        self.body = nn.Sequential(*m)
        self.res_scale = res_scale

    def forward(self, x):
        res = self.body(x)
        res += x
        return res * self.res_scale


#%% Class for defining EDSR Network
class EDSR(nn.Module):

    """ Defines EDSR network

        Params
        --------
            scale_factor (int):     Scale factor for upscaling
            num_channels (int):     Number of channels
            num_feats (int):        Number of features
            num_blocks (int):       Number of ResNet blocks
            res_scale (float):      Scale factor for ResNet weights

    """

    def __init__(
        self,
        scale_factor: int=2,
        num_channels: int=3,
        num_feats: int=64,
        num_blocks: int=16,
        res_scale: float=1.0
    ):
        super(EDSR, self).__init__()
        self.head = nn.Conv2d(num_channels, num_feats, kernel_size=(3, 3), padding=3//2)
        body = [ResBlock(num_feats, res_scale) for _ in range(num_blocks)]
        body.append(nn.Conv2d(num_feats, num_feats, kernel_size=(3, 3), stride=(1, 1), padding=1))
        self.body = nn.Sequential(*body)
        self.upscale = nn.Sequential(
            nn.Conv2d(num_feats, num_feats * (scale_factor ** 2), kernel_size=(3, 3), stride=(1, 1), padding=1),
            nn.PixelShuffle(scale_factor)
        )
        self.tail = nn.Sequential(nn.Conv2d(num_feats, num_channels, kernel_size=(3, 3), stride=(1, 1), padding=1))
    
    @amp.autocast()
    def forward(self, x):
        x = self.head(x)
        res = self.body(x)
        res += x
        x = self.upscale(res)
        x = self.tail(x)
        return x