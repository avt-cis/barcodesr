'''
@Description: Module for PyTorch logging.
@Developed by: Alex Choi
@Date: Nov. 18, 2020
@Contact: alex.choi@cognex.com
'''

#%% Importing libraries
import logging


#%% Logging
level = logging.INFO
logging.basicConfig(level=level, format='%(asctime)s - [%(levelname)s] - %(message)s')