'''
@Description: Module for utility functions.
@Developed by: Alex Choi
@Date: Aug. 09, 2021
@Contact: alex.choi@cognex.com
'''


#%% Importing libraries
import os
import yaml
import numpy as np
import PIL.Image as pil_image
import torch
from logger import logging

import neptune
import json

from models import EDSR


# %% function to get the configurations from YAML
def get_configurations(config_yaml_path: str) -> dict:

    """ Gets compute device

    Params
    --------
        config_yaml_path (str): YAML configuration path

    Returns
    --------
        configs (dict): Configurations read from YAML

    """

    with open(config_yaml_path) as f:
        configs = yaml.load(f, Loader=yaml.FullLoader)

    if os.name == 'nt':
        configs['train']['num_workers'] = 0

    return configs


# %% device settings
def get_device(compute_device: str) -> str:

    """ Gets compute device

    Params
    --------
        compute_device (str): Device name

    Returns
    --------
        device (str): Compute device (cpu or cuda)

    """

    # Available device
    device_available = 'cuda' if torch.cuda.is_available() else 'cpu'

    if compute_device == 'cpu':
        device = 'cpu'
    elif compute_device == 'gpu':
        if device_available == 'cuda': device = 'cuda'
    else:  # default option
        device = device_available

    #logging.info(f'[Compute Device] Device: {device}')

    return device


#%% function to check image
def check_image_file(filename: str):

    """ Checks the validity of the image file extension

    Params
    --------
        filename (str): Filename (including path)

    Returns
    --------
        Valid image filename

    """

    return any(
        filename.endswith(extension) for extension in [
            ".jpg", ".jpeg", ".png", ".bmp", ".tif", ".tiff", ".JPG", ".JPEG", ".PNG", ".BMP"
        ]
    )


#%% function for pre-processing an image
def preprocess(img: pil_image) -> torch.tensor:

    """ Pre-processing for the image

    Params
    --------
        img (pil_image): Pillow image

    Returns
    --------
        x (torch.tensor): Converted tensor from the image

    """

    # Converting: uInt8 -> float32
    x = np.array(img).astype(np.float32)

    if len(x.shape) < 3:
        x = np.expand_dims(x, axis=-1)

    x = x.transpose([2, 0, 1])

    # Normalizing x
    x /= 255.

    x = torch.from_numpy(x)
    x = x.unsqueeze(0)

    return x


#%% function for postprocessing a tensor
def postprocess(tensor: torch.tensor):

    """ Post-processing for the tensor (Conversion of PyTorch tensor to numpy array)

    Params
    --------
        tensor (torch.tensor): Filename (including path)

    Returns
    --------
        x (numpy.array): Converted numpy array from torch tensor

    """

    x = tensor.mul(255.0).cpu().numpy().squeeze(0)
    x = np.array(x).transpose([1,2,0])

    # special handling for 1-ch depth
    if tensor.shape[1] == 1: x = np.squeeze(x, -1)

    x = np.clip(x, 0.0, 255.0).astype(np.uint8)
    return x


#%% function to calculate PSNR value
def calc_psnr(img1: torch.tensor, img2: torch.tensor):

    """ Calculates the PSNR value between 2 images

    Params
    --------
        img1 (torch.tensor):    image 1
        img2 (torch.tensor):    image 2

    Returns
    --------
        pnsr_val (torch.tensor): PSNR value

    """

    pnsr_val = 10. * torch.log10(1. / torch.mean((img1 - img2) ** 2))
    return pnsr_val


#%% Class: AverageMeter
class AverageMeter(object):

    """ Computes and stores the average and current value

    Params
    --------
        name (str):     Name of the metrics
        fmt (str):      Format

    """

    def __init__(self, name: str, fmt: str=':f'):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val: torch.tensor, n: int=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def __str__(self):
        fmtstr = '{name} {val' + self.fmt + '} ({avg' + self.fmt + '})'
        return fmtstr.format(**self.__dict__)


#%% Class: ProgressMeter
class ProgressMeter(object):

    """ Displays the progress """

    def __init__(self, num_batches: int, meters: list, prefix: str=""):
        self.batch_fmtstr = self._get_batch_fmtstr(num_batches)
        self.meters = meters
        self.prefix = prefix

    def display(self, batch):
        entries = [self.prefix + self.batch_fmtstr.format(batch)]
        entries += [str(meter) for meter in self.meters]
        print('\t'.join(entries))

    def _get_batch_fmtstr(self, num_batches):
        num_digits = len(str(num_batches // 1))
        fmt = '{:' + str(num_digits) + 'd}'
        return '[' + fmt + '/' + fmt.format(num_batches) + ']'


#%% function to get the trained model
def get_trained_model(
    model_path: str,
    scale_factor: int,
    num_channels: int,
    num_feats: int,
    num_blocks: int,
    res_scale: float,
    device: str='cuda',
) -> EDSR:

    """ Gets the trained model

    Params
    --------
        model_path (str):       path to the trained model
        scale_factor (int):     scale factor for super-resolution upscaling
        num_channels (int):     number of image channels
        num_feats (int):        number of features
        num_blocks (int):       number of residual blocks
        res_scale (float):      scale factor of residual block
        device (str):           computing device ('cuda' or 'cpu')

    Returns
    --------
        model (EDSR):           EDSR trained model

    """

    model = EDSR(
        scale_factor=scale_factor,
        num_channels=num_channels,
        num_feats=num_feats,
        num_blocks=num_blocks,
        res_scale=res_scale
    ).to(device)

    try:
        model.load_state_dict(torch.load(model_path, map_location=device))
    except:
        state_dict = model.state_dict()
        for n, p in torch.load(
            model_path,
            map_location=lambda storage, loc: storage
        )['model_state_dict'].items():
            if n in state_dict.keys():
                state_dict[n].copy_(p)
            else:
                raise KeyError(n)

    return model


#%% function to test one image
def test_one_image(
    test_image_path: str,
    num_channels: int,
    scale: int,
    model: EDSR,
    interpolation_save_path: str,
    edsr_save_path: str,
    device: str='cuda'
):

    """ Gets the trained model

    Params
    --------
        test_image_path (str):              path to the test image
        num_channels (int):                 number of image channels (1 or 3)
        interpolation_save_path (str):      path to save the interpolation result image (bicubic, bilinear, ...)
        edsr_save_path (str):               path to the EDSR result image
        device (str):                       computing device ('cuda' or 'cpu')

    Returns
    --------
        None

    """

    ## Evaluation mode of the model
    model.eval()

    # Loading the image
    image = pil_image.open(test_image_path).convert('RGB') if num_channels == 3 \
        else pil_image.open(test_image_path).convert('L')

    image_width = (image.width // scale) * scale
    image_height = (image.height // scale) * scale

    # Generating the Bicubic interpolation image
    hr = image.resize((image_width, image_height), resample=pil_image.BICUBIC)
    lr = hr.resize(
        (hr.width // scale, hr.height // scale),
        resample=pil_image.BICUBIC
    )
    bicubic = lr.resize(
        (lr.width * scale, lr.height * scale),
        resample=pil_image.BICUBIC
    )

    lr = preprocess(lr).to(device)
    hr = preprocess(hr).to(device)
    bic = preprocess(bicubic).to(device)

    # Generating the EDSR image
    with torch.no_grad():
        pred_sr = model(lr)

    output = postprocess(pred_sr)
    image = pil_image.fromarray(output)

    # Calculating the PSNR values
    sr_psnr = calc_psnr(hr, pred_sr)
    bic_psnr = calc_psnr(hr, bic)

    logging.info(f"EDSR PSNR: {sr_psnr:.2f}, BICUBIC PSNR: {bic_psnr:.2f}")

    # Saving images
    bicubic.save(interpolation_save_path)
    image.save(edsr_save_path)

    return

#%% neptune experiment
def init_experiment(configs: dict) -> neptune.experiments.Experiment:

    """ Gets neptune experiment for monitoring

    Params
    --------
        config_yaml_path (str): YAML configuration path

    Returns
    --------
        neptune_exp (dict): Neptune experiment

    """

    secret_json_path = configs["monitoring"]["neptune-secret-path"]

    with open(secret_json_path) as json_file:
        secret = json.load(json_file)

    neptune.init(
        project_qualified_name=secret["project_qualified_name"],
        api_token=secret["neptune_api_token"]
    )

    # Create an experiment
    print('\n')
    logging.info('Initializing Neptune monitoring instance....')
    neptune_exp = neptune.create_experiment(params=configs)
    logging.info('Finished initializing Neptune monitoring instance.\n')

    return neptune_exp