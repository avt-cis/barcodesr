'''
@Description: Module for dataset.
@Developed by: Alex Choi
@Date: Aug. 09, 2021
@Contact: alex.choi@cognex.com
'''

#%% Import libraries
import os
import random
from PIL import Image

import torchvision.transforms as transforms
import torchvision.transforms.functional as F

from utils import check_image_file


#%% Class for Custom Dataset
class CustomDataset(object):

    """ Defines Custom Dataset

        Params
        --------
            images_dir (str):       Path to images
            image_size (int):       Size of image cropping (patch size)
            num_channels (int):     Number of channels for input image
            upscale_factor (int):   Scale factor for upscaling

    """

    def __init__(
        self,
        images_dir: str,
        image_size: int,
        num_channels: int,
        upscale_factor: int
    ):
        self.filenames = [
            os.path.join(images_dir, x) for x in os.listdir(images_dir) if check_image_file(x)
        ]

        self.image_size = image_size
        self.num_channels = num_channels
        self.upscale_factor = upscale_factor

        self.interpolation_options = [
            F.InterpolationMode.BICUBIC,
            F.InterpolationMode.BILINEAR,
            F.InterpolationMode.HAMMING,
            F.InterpolationMode.LANCZOS,
            F.InterpolationMode.NEAREST,
            F.InterpolationMode.BOX
        ]

    def __getitem__(self, idx):
        # transform for low resolution image
        if self.num_channels == 1:
            lr_transforms = transforms.Compose([
                transforms.ToPILImage(),
                transforms.Grayscale(num_output_channels=1),
                transforms.Resize(
                    (self.image_size // self.upscale_factor, self.image_size // self.upscale_factor),
                    interpolation=random.choice(self.interpolation_options)
                ),
                transforms.ToTensor()
            ])
            # transform for high resolution image
            hr_transforms = transforms.Compose([
                transforms.Grayscale(num_output_channels=1),
                transforms.RandomCrop((self.image_size, self.image_size)),
                # transforms.RandomHorizontalFlip(),
                # transforms.RandomVerticalFlip(),
                transforms.AutoAugment(),
                transforms.ToTensor()
            ])

        elif self.num_channels == 3:
            lr_transforms = transforms.Compose([
                transforms.ToPILImage(),
                transforms.Resize(
                    (self.image_size // self.upscale_factor, self.image_size // self.upscale_factor),
                    interpolation=random.choice(self.interpolation_options)
                ),
                transforms.ToTensor()
            ])
            # transform for high resolution image
            hr_transforms = transforms.Compose([
                transforms.RandomCrop((self.image_size, self.image_size)),
                # transforms.RandomHorizontalFlip(),
                # transforms.RandomVerticalFlip(),
                transforms.AutoAugment(),
                transforms.ToTensor()
            ])

        else:
            raise ValueError("Invalid number of channels for input image")

        hr = hr_transforms(Image.open(self.filenames[idx]).convert("RGB"))
        lr = lr_transforms(hr)
        return lr, hr

    def __len__(self):
        return len(self.filenames)