#%% Import libraries
import os
import time
import yaml
import streamlit as st
import streamlit.components.v1 as components
from bokeh.plotting import figure

from logger import logging

from models import EDSR
from data import CustomDataset
from tqdm import tqdm
from PIL import Image

import torch
import torch.backends.cudnn as cudnn
import torchvision.utils as vutils
from torch.utils.data.dataloader import DataLoader
from torch import nn
from torch.cuda import amp
from torch.optim import lr_scheduler

from utils import (
    get_device,
    AverageMeter,
    ProgressMeter,
    calc_psnr,
    preprocess,
    init_experiment
)

from user_interface import ui_sr_trainer


#%% Configurations
detection_config_path = './config/detection-configs.yml'
sr_config_path = './config/sr-configs.yml'
state_info_path = './config/state.yml'


#%% functions to read and write the initial configurations
def read_configurations():
    with open(sr_config_path) as f:
        configs = yaml.load(f, Loader=yaml.FullLoader)

    return configs

def write_configurations(configs, mode):
    if mode == 'sr':
        with open(sr_config_path, 'w') as yml:
            yaml.dump(configs, yml, allow_unicode=True)
    elif mode == 'detection':
        with open(detection_config_path, 'w') as yml:
            yaml.dump(configs, yml, allow_unicode=True)

    return


#%% function to delete files
def delete_output_files(configs):
    target_dir = os.path.join(configs['outputs']['root_path'], f"EDSR_x{configs['model']['scale']}")
    if os.path.exists(target_dir):
        file_list = os.listdir(target_dir)
        for filename in file_list:
            target_path = os.path.join(target_dir, filename)
            os.remove(target_path)
    return


#%% function to train
def train_sr(configs):
    logging.info("function train_sr() called.")

    ## Initialize the Neptune experiment
    neptune_exp = init_experiment(configs)

    components.html("""<hr/>""")

    ## Creating output directory
    st.write("[INFO] Creating the output directories.")
    outputs_dir = os.path.join(configs['outputs']['root_path'], f"EDSR_x{configs['model']['scale']}")
    if not os.path.exists(outputs_dir):
        os.makedirs(outputs_dir)
    st.write("[INFO] Finished creating the output directories.")

    ## Setting computing device
    cudnn.benchmark = True
    device = get_device(compute_device=configs['device'])
    logging.info(f"Device: {device}")

    ## Setting torch seed
    torch.manual_seed(configs['train']['seed'])

    ## Testing an image
    test_image_path = configs['test']['example_image_path']
    if configs['model']['num_channels'] == 1:
        test_image = Image.open(test_image_path).convert('L')
    elif configs['model']['num_channels'] == 3:
        test_image = Image.open(test_image_path).convert('RGB')
    else:
        raise ValueError("Invalid number of channels.")

    test_image = preprocess(test_image)


    ## Loading the model
    model = EDSR(
        scale_factor=configs['model']['scale'],
        num_channels=configs['model']['num_channels'],
        num_feats=configs['model']['num_feats'],
        num_blocks=configs['model']['num_blocks'],
        res_scale=configs['model']['res_scale']
    ).to(device)

    logging.info("Model created.")
    st.write("[INFO] EDSR model is created.")

    ## Loss and Optimizer
    pixel_criterion = nn.L1Loss().to(device)
    psnr_optimizer = torch.optim.Adam(
        model.parameters(),
        float(configs['train']['psnr_lr']),
        (0.9, 0.999)
    )

    total_epoch = configs['train']['num_epochs']
    start_epoch = 0
    best_psnr = 0

    ## Loading weights from an existing checkpoint file
    if configs['train']['from'] == 'checkpoint':
        if os.path.exists(configs['train']['checkpoint']):
            checkpoint = torch.load(configs['train']['checkpoint'])
            model.load_state_dict(checkpoint['model_state_dict'])
            psnr_optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
            start_epoch = checkpoint['epoch'] + 1
            loss = checkpoint['loss']
            best_psnr = checkpoint['best_psnr']

    ## Printing train settings information
    components.html("""<hr/>""")
    st.write("**_EDSR MODEL INFO_**")
    st.write(f" * Upscaling Factor: {configs['model']['scale']}")
    st.write(f" * Number of channels: {configs['model']['num_channels']}")
    st.write(f" * Number of blocks: {configs['model']['num_blocks']}")
    st.write(f" * Number of features: {configs['model']['num_feats']}")
    st.write(f" * Residual scale: {configs['model']['res_scale']}")
    st.write("**_EDSR TRAINING INFO_**")
    st.write(f" * Total Epoch: {configs['train']['num_epochs']}")
    st.write(f" * Start Epoch: {start_epoch}")
    st.write(f" * Train directory path: {configs['data']['train_path']}")
    st.write(f" * Test directory path: {configs['data']['valid_path']}")
    st.write(f" * Output weights directory path: {configs['outputs']['root_path']}")
    st.write(f" * PSNR learning rate: {configs['train']['psnr_lr']}")
    st.write(f" * Patch size: {configs['model']['patch_size']}")
    st.write(f" * Batch size: {configs['train']['batch_size']}")
    components.html("""<hr/>""")

    ## Setting learning scheduler
    psnr_scheduler = lr_scheduler.StepLR(
        psnr_optimizer,
        step_size=30,
        gamma=0.1
    )
    scaler = amp.GradScaler()

    ## Setting dataset
    train_dataset = CustomDataset(
        images_dir=configs['data']['train_path'],
        image_size=configs['model']['patch_size'],
        num_channels=configs['model']['num_channels'],
        upscale_factor=configs['model']['scale']
    )
    train_dataloader = DataLoader(
        dataset=train_dataset,
        batch_size=configs['train']['batch_size'],
        shuffle=configs['train']['shuffle'],
        num_workers=configs['data_pipeline']['num_workers'],
        pin_memory=configs['data_pipeline']['pin_memory']
    )

    eval_dataset = CustomDataset(
        images_dir=configs['data']['valid_path'],
        image_size=configs['model']['patch_size'],
        num_channels=configs['model']['num_channels'],
        upscale_factor=configs['model']['scale']
    )
    eval_dataloader = DataLoader(
        dataset=eval_dataset,
        batch_size=configs['valid']['batch_size'],
        shuffle=configs['valid']['shuffle'],
        num_workers=configs['data_pipeline']['num_workers'],
        pin_memory=configs['data_pipeline']['pin_memory']
    )

    ## Start training and testing
    st.write("[INFO] Training started...")
    start_train_time = time.time()
    history = {
        "epoch": [],
        "train_loss": [],
        "train_psnr": [],
        "valid_loss": [],
        "valid_psnr": []
    }
    for epoch in tqdm(range(start_epoch, total_epoch)):
        with open(state_info_path) as f:
            train_state = yaml.load(f, Loader=yaml.FullLoader)

        if not train_state['train']:
            st.write("[INFO] Stopped training by user.")
            del model
            del lr
            del hr
            del preds
            del loss
            del psnr_val
            del loss
            del avg_psnr_val
            torch.cuda.empty_cache()
            break

        model.train()
        losses = AverageMeter(name="PSNR Loss", fmt=":.6f")
        psnr = AverageMeter(name="PSNR", fmt=":.6f")
        progress = ProgressMeter(
            num_batches=len(train_dataloader),
            meters=[losses, psnr],
            prefix=f"Epoch: [{epoch}]"
        )

        # training epoch
        psnr_loss_sum = 0.0
        psnr_val_sum = 0.0
        for i, (lr, hr) in enumerate(train_dataloader):
            lr = lr.to(device)
            hr = hr.to(device)

            psnr_optimizer.zero_grad()

            with amp.autocast():
                preds = model(lr)
                loss = pixel_criterion(preds, hr)

            scaler.scale(loss).backward()
            scaler.step(psnr_optimizer)
            scaler.update()

            losses.update(loss.item(), len(lr))
            psnr_val = calc_psnr(preds, hr)
            psnr.update(psnr_val, len(lr))

            psnr_loss_sum += loss.item()
            psnr_val_sum += psnr_val

            if i % 100 == 0:
                progress.display(i)

        avg_psnr_loss = psnr_loss_sum / (i + 1)
        avg_psnr_val = psnr_val / (i + 1)
        st.write(f"[Train] Epoch: {epoch}, PSNR Loss: {avg_psnr_loss:4f}, PSNR: {avg_psnr_val:.4f}")

        history['epoch'].append(epoch)
        history['train_loss'].append(avg_psnr_loss)
        history['train_psnr'].append(avg_psnr_val.cpu().detach().numpy())

        neptune_exp.log_metric("train_loss", avg_psnr_loss)
        neptune_exp.log_metric("train_psnr", avg_psnr_val.cpu().detach().numpy())

        psnr_scheduler.step()

        # testing epoch
        model.eval()
        with torch.no_grad():
            psnr_loss_sum = 0.0
            psnr_val_sum = 0.0
            for i, (lr, hr) in enumerate(eval_dataloader):
                lr = lr.to(device)
                hr = hr.to(device)
                preds = model(lr)
                loss = pixel_criterion(preds, hr)
                psnr_val = calc_psnr(preds, hr)
                psnr_loss_sum += loss.item()
                psnr_val_sum += psnr_val

            avg_psnr_loss = psnr_loss_sum / (i + 1)
            avg_psnr_val = psnr_val / (i + 1)
            st.write(f"[Validation] Epoch: {epoch}, PSNR Loss: {avg_psnr_loss:4f}, PSNR: {avg_psnr_val:.4f}")

            history['valid_loss'].append(avg_psnr_loss)
            history['valid_psnr'].append(avg_psnr_val.cpu().detach().numpy())

            neptune_exp.log_metric("valid_loss", avg_psnr_loss)
            neptune_exp.log_metric("valid_psnr", avg_psnr_val.cpu().detach().numpy())

        if psnr.avg > best_psnr:
            best_psnr = psnr.avg
            torch.save(
                model.state_dict(),
                os.path.join(outputs_dir, 'best.model')
            )

        torch.save(
            {
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': psnr_optimizer.state_dict(),
                'loss': loss,
                'best_psnr': best_psnr,
            },
            os.path.join(outputs_dir, 'epoch_{}.pth'.format(epoch))
        )

        # test with an example barcode cropped image
        with torch.no_grad():
            lr = test_image.to(device)
            preds = model(lr)
            image_save_path = os.path.join(outputs_dir, f"PSNR_{epoch + 1}.png")
            vutils.save_image(preds.detach(), image_save_path)
            st.image(image_save_path, caption=f"Epoch: {epoch + 1}")

    end_train_time = time.time()
    st.write(f"[INFO] Training finished. Elapsed train time: {(end_train_time - start_train_time):.3f} (sec).")

    # Plotting history
    p = figure(
        title = 'PSNR Loss History',
        x_axis_label = 'Epoch',
        y_axis_label = 'PSNR Loss'
    )
    p.line(history['epoch'], history['train_loss'], legend_label='Train Loss', line_width=2)
    p.line(history['epoch'], history['valid_loss'], legend_label='Validation Loss', line_width=2)
    st.bokeh_chart(p, use_container_width=True)

    p = figure(
        title='PSNR Value History',
        x_axis_label='Epoch',
        y_axis_label='PSNR Value'
    )
    p.line(history['epoch'], history['train_psnr'], legend_label='Train PSNR', line_width=2)
    p.line(history['epoch'], history['valid_psnr'], legend_label='Validation PSNR', line_width=2)
    st.bokeh_chart(p, use_container_width=True)

    torch.cuda.empty_cache()
    return


#%% function main
def main():
    ## initialization
    configs = read_configurations()

    # /////////////////////////////////////////////////////////////////////////
    # Main panel
    # /////////////////////////////////////////////////////////////////////////

    st.header('**_Barcode Super-resolution Training Application_**')
    st.write('**_Author_**: Alex Choi')
    st.write('**_Version_**: 1.0')
    st.write('**_Contact_**: alex.choi@cognex.com')
    st.write('ⓒ2021 Cognex. All Rights Reserved.')

    # /////////////////////////////////////////////////////////////////////////
    # Side panel
    # /////////////////////////////////////////////////////////////////////////

    st.sidebar.header('**_Select Problem & Mode_**')
    selected_option = st.sidebar.selectbox(
        'Problem',
        ('Barcode Detection', 'Super-resolution')
    )

    if selected_option == 'Barcode Detection':
        selected_mode = st.sidebar.selectbox(
            'Mode',
            ('Trainer', 'Predictor')
        )
        if selected_mode == 'Trainer':
            pass
        elif selected_mode == 'Predictor':
            pass
    elif selected_option == 'Super-resolution':
        selected_mode = st.sidebar.selectbox(
            '',
            ('Trainer', 'Predictor')
        )
        if selected_mode == 'Trainer':
            configs = ui_sr_trainer(configs)
            if st.sidebar.button('Check the Train Options'):
                options_valid = True
                if not os.path.exists(configs['data']['train_path']): options_valid = False
                if not os.path.exists(configs['data']['valid_path']): options_valid =  False
                if not os.path.exists(configs['test']['example_image_path']): options_valid = False
                if options_valid:
                    logging.info('Options are valid.')

                else:
                    components.html(
                        """
                        <h3 style="color:Red;">[ERROR] Invalid path(s) defined!</h3>
                        """
                    )

                st.write(configs)

            if st.sidebar.button('Start Training'):
                write_configurations(configs, mode='sr')
                delete_output_files(configs)
                logging.info('[Start Training] button clicked.')
                # set train state to True
                with open(state_info_path, 'w') as yml:
                    yaml.dump({'train': True}, yml, allow_unicode=True)
                train_sr(configs)

            if st.sidebar.button('Stop Training'):
                # set train state to True
                with open(state_info_path, 'w') as yml:
                    yaml.dump({'train': False}, yml, allow_unicode=True)

        elif selected_mode == 'Predictor':
            pass


    return


#%% main
if __name__ == "__main__":
    main()

