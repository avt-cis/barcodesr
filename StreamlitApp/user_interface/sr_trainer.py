import os
import streamlit as st

from utils import (
    get_device
)

def ui_sr_trainer(configs):
    ##############################################################
    # Data
    ##############################################################
    st.sidebar.header('**_Data_**')
    train_path = st.sidebar.text_input('Train Dataset Path')
    valid_path = st.sidebar.text_input('Validation Dataset Path')

    train_path = train_path.replace(os.sep, '/')
    valid_path = valid_path.replace(os.sep, '/')

    configs['data']['train_path'] = train_path
    configs['data']['valid_path'] = valid_path

    ##############################################################
    # Device
    ##############################################################
    st.sidebar.header('**_Device_**')
    selected_device = st.sidebar.selectbox(
        'Select the Compute Device',
        ('GPU', 'CPU')
    )
    device = get_device(compute_device=selected_device)
    configs['device'] = selected_device.lower()

    ##############################################################
    # Data Pipeline
    ##############################################################
    st.sidebar.header('**_Data Pipeline_**')
    num_workers = st.sidebar.slider(
        'Number of Data Processors',
        0, 8, 0
    )
    if os.name == 'nt': num_workers = 0
    use_pin_memory = st.sidebar.radio(
        "Use Pin Memory",
        ('Use', 'Not Use')
    )
    configs['data_pipeline']['num_workers'] = num_workers
    configs['data_pipeline']['pin_memory'] = True if use_pin_memory == 'Use' else False

    ##############################################################
    # Model
    ##############################################################
    st.sidebar.header('**_Model_**')
    upscale = st.sidebar.selectbox(
        'Upscale Factor',
        ('2x', '4x')
    )
    num_channels = st.sidebar.selectbox(
        'Number of Channels',
        ('1', '3')
    )
    num_feats = st.sidebar.slider(
        'Number of Features in Residual Block',
        32, 1024, 128
    )
    num_blocks = st.sidebar.slider(
        'Number of Residual Blocks',
        4, 64, 16
    )
    res_scale = st.sidebar.slider(
        'Residual Scaling',
        0.1, 1.0, 0.1
    )
    patch_size = st.sidebar.number_input('Image Patch Size (32 - 1024)', 32, 1024, 368)

    configs['model']['scale'] = 2 if upscale == '2x' else 4
    configs['model']['num_channels'] = 1 if num_channels == '1' else 3
    configs['model']['num_feats'] = num_feats
    configs['model']['num_blocks'] = num_blocks
    configs['model']['res_scale'] = res_scale
    configs['model']['patch_size'] = patch_size

    ##############################################################
    # Train
    ##############################################################
    st.sidebar.header('**_Train_**')
    train_from = st.sidebar.selectbox(
        'Train from',
        ('Scratch', 'Checkpoint')
    )
    if train_from == 'Checkpoint':
        checkpoint_path = st.sidebar.text_input('Checkpoint Path')
        checkpoint_path = checkpoint_path.replace(os.sep, '/')
        configs['train']['checkpoint'] = checkpoint_path
    learning_rate = st.sidebar.number_input('Learning Rate (0.00001 - 0.1)', 0.00001, 0.1, 0.001, format='%10.8f')
    train_batch_size = st.sidebar.number_input('Batch Size (2 - 128)', 2, 128, 16)
    num_epochs = st.sidebar.number_input('Number of Epochs (1 - 500)', 1, 500, 100)
    train_shuffle = st.sidebar.radio(
        "Shuffle Train Data",
        ('Yes', 'No')
    )

    configs['train']['from'] = train_from.lower()
    configs['train']['psnr_lr'] = learning_rate
    configs['train']['batch_size'] = train_batch_size
    configs['train']['num_epochs'] = num_epochs
    configs['train']['num_epochs'] = num_epochs
    configs['train']['shuffle'] = True if train_shuffle == 'Yes' else False

    ##############################################################
    # Validation
    ##############################################################
    st.sidebar.header('**_Validation_**')
    valid_batch_size = st.sidebar.number_input('Batch Size (2 - 64)', 2, 128, 8)
    valid_shuffle = st.sidebar.radio(
        "Shuffle Validation Data",
        ('Yes', 'No')
    )

    configs['valid']['batch_size'] = valid_batch_size
    configs['valid']['shuffle'] = True if valid_shuffle == 'Yes' else False

    ##############################################################
    # Test
    ##############################################################
    st.sidebar.header('**_Test_**')
    test_image_path = st.sidebar.text_input('Test Image Path')
    test_image_path = test_image_path.replace(os.sep, '/')

    configs['test']['example_image_path'] = test_image_path

    return configs